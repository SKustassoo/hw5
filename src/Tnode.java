import java.util.*;

public class Tnode {

   private String name;
   private Tnode firstChild;
   private Tnode nextSibling;
   
   
   //Konstruktor et saaks kasutada toString meetodit, mis lisab sulud ja komad
   public Tnode(String token){
	   name = token;
   }
  

   @Override
   public String toString() {
      StringBuffer b = new StringBuffer();
      b.append(name);
      
      if(this.firstChild != null)
    	  b.append("(" + this.firstChild.toString());
      if(this.nextSibling != null)
    	  b.append(","+ this.nextSibling.toString()+")");

      return b.toString();
   }

   public static Tnode buildFromRPN (String pol) {
	   if(pol.isEmpty()){
		throw new RuntimeException("Puuduvad elemendid, millest lahendit moodustada.");   
	   }
	   
	   StringTokenizer st = new StringTokenizer(pol, " \t");
	   Stack<Tnode> theStack = new Stack<Tnode>();

	   int i = 0;
	   int pikkus = st.countTokens();
	   Tnode root;
	  
	   while(st.hasMoreTokens()){
		   String token = (String) st.nextElement();

		   
		   if(token.equals("+") || token.equals("-") || token.equals("/")|| token.equals("*")){
			   if(theStack.size() <= 2){
				   throw new RuntimeException("Nimekirjas ("+pol+") ei ole piisavalt elemente, et teostada tehet." );
			   }
			  /* else if(theStack.size() == 1)
				   throw new RuntimeException();*/
			   else{
				   root = new Tnode(token);
				   root.nextSibling = theStack.pop();
				   root.firstChild = theStack.pop();
				   theStack.push(root.nextSibling);
				   theStack.push(root.firstChild);
			   }
			   
		   }else{
			   if(i == pikkus && i>2) {
				   throw new RuntimeException();
			   }
			   
			  
		   }
		   //i++;
	   }
      
      root = theStack.pop();
      return root;
   }

   public static void main (String[] param) {
      String rpn = "1 2 +";
      System.out.println ("RPN: " + rpn);
      Tnode res = buildFromRPN (rpn);
      System.out.println ("Tree: " + res);
      // TODO!!! Your tests here
   }
}

